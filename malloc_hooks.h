#ifndef MALLOC_HOOKS_H
#define MALLOC_HOOKS_H

#include <malloc.h>
#include <map>
#include <vector>
#include <string>

//#define MALLOC_HOOK_LOGS
//#define MALLOC_HOOK_ENABLE_DISABLE_LOGS
#define MALLOC_HOOK_TO_FILE_LOGS

static void *(*old_malloc_hook) (size_t, const void *);
static void *(*old_free_hook) (void*, const void *);
static void *(*old_realloc_hook) (void*, size_t, const void *);

// Prototypes for our hooks.
static void my_init_hook (void);
static void *my_malloc_hook (size_t, const void *);
static void my_free_hook (void*, const void *);
static void *my_realloc_hook(void *ptr, size_t size, const void *caller);

// Additional hook functions
void OldToStandart();
void StandartToMy();
void StandartToOld();

// Hook enabling
void my_init (void);

//Hook disabling
void DisableHooks();
void EnableHooks();

// Hooks implementations
void * my_malloc_hook (size_t size, const void *caller);
void my_free_hook (void *ptr, const void *caller);
void * my_realloc_hook (void *ptr, size_t size, const void *caller);

typedef std::map< void *, std::size_t, std::less<void*> > PtrBytes;
typedef std::map< void *, std::string, std::less<void*> > PtrBacktrace;

PtrBytes * GetPtrBytes();
PtrBacktrace *GetPtrBacktrace();

void DumpPtrBytesMap();
void DumpPtrBacktraceMap();

void PtrBytesMallocAction(void *ptr, size_t size);
void PtrBytesReallocTrigger(void *ptr, void* result, size_t size);
void PtrBytesFreeTrigger(void *ptr);

void PtrBacktraceMallocAction(void *ptr, std::string str);
void PtrBacktraceReallocTrigger(void *ptr, void* result, std::string str);
void PtrBacktraceFreeTrigger(void *ptr);

typedef std::map< std::string, std::size_t, std::less<std::string> > MapBacktraceSize;
typedef std::vector< std::pair<std::string, std::size_t> > VecBacktraceSize;

// Prints Backtrace | Allocation size table
void PrintStat(int curRss, int prevRss);

int GetProcessRss();
void CheckMemoryConsumption();
#endif
