#include "malloc_hooks.h"
#include <cstdio>
#include <cstdlib>
#include <string>
#include <execinfo.h>
#include <utility>
#include <algorithm>
#include <ctime>
#include <errno.h>
#include <Utility.h>
#include <QPixmapCache>

const int gBtBufSize = 255;
const int gPrintTimeout = 0.5; // seconds
const int gAddrCount = 11; // 6 last frames
const std::string gLogFileName = "/home/admin/mem_consumption.txt";
int gRss = 0;
int gLogFileCount = 1;

PtrBytes * GetPtrBytes()
{
    static PtrBytes * ptrBytesTrack = new (malloc(sizeof *ptrBytesTrack)) PtrBytes;
    return ptrBytesTrack;
}

PtrBacktrace *GetPtrBacktrace()
{
    static PtrBacktrace * ptrBacktraceTrack = new (malloc(sizeof *ptrBacktraceTrack)) PtrBacktrace;
    return ptrBacktraceTrack;
}

void OldToStandart()
{
    old_malloc_hook = __malloc_hook;
    old_free_hook = __free_hook;
    old_realloc_hook = __realloc_hook;
}

void StandartToMy()
{
    __malloc_hook = my_malloc_hook;
    __free_hook = my_free_hook;
    __realloc_hook = my_realloc_hook;
}

void StandartToOld()
{
    __malloc_hook = old_malloc_hook;
    __free_hook = old_free_hook;
    __realloc_hook = old_realloc_hook;
}

void DisableHooks()
{
#ifdef MALLOC_HOOK_ENABLE_DISABLE_LOGS
    printf("malloc_hooks logs: disable my hooks\n");
#endif
    StandartToOld();
}

//static timespec now = Now();
//static timespec last = Now();

void my_init (void)
{
#ifdef MALLOC_HOOK_ENABLE_DISABLE_LOGS
    printf("malloc_hooks logs: enable my hooks\n");
#endif
    OldToStandart();
    StandartToMy();
}

void * my_malloc_hook (size_t size, const void *caller)
{
    void* result;
    void *buffer[gBtBufSize];
    char **strings;

    /* Restore all old hooks */
    StandartToOld();

    /* Call recursively malloc */
    result = malloc(size);

    /* Save underlying hooks */
    OldToStandart();

    // Get Backtrace
    int nptrs = backtrace(buffer, gBtBufSize);
    strings = backtrace_symbols(buffer, nptrs);
    if (strings == NULL)
    {
        perror("backtrace_symbols");
        exit(EXIT_FAILURE);
    }

    int end;
    if (nptrs <= gAddrCount)
        end = nptrs;
    else
        end = gAddrCount;

#ifdef MALLOC_HOOK_LOGS
    printf("\nmalloc_hooks logs: malloc (%u) returns %p\n", (unsigned int) size, result);
#endif

    std::string backtraceStr;
    for (int n = 2; n < end; n++)
    {
        backtraceStr.append(strings[n]);
        backtraceStr.append("\n");
    }

#ifdef MALLOC_HOOK_LOGS
    printf("backtrace: %s\n", backtraceStr.c_str());
#endif
    free(strings);

    // Save to map<pointer,bytes>
    PtrBytesMallocAction(result, size);
    PtrBacktraceMallocAction(result, backtraceStr);

    /* Restore our own hooks */
    StandartToMy();
    return result;
}

void my_free_hook (void *ptr, const void *caller)
{
    if (!ptr)
        return;
    /* Restore all old hooks */
    StandartToOld();

    /* Call recursively */
    free (ptr);

    /* Save underlying hooks */
    OldToStandart();

#ifdef MALLOC_HOOK_LOGS
    // Too much logs
    printf ("\nmalloc_hooks logs: freed pointer %p\n", ptr);
#endif

    // Nullify size of the pointer
    PtrBytesFreeTrigger(ptr);
    PtrBacktraceFreeTrigger(ptr);

    /* Restore our own hooks */
    StandartToMy();
}

void * my_realloc_hook (void *ptr, size_t size, const void *caller)
{
    void *result;

    /* Restore all old hooks */
    StandartToOld();

    /* Call recursively */
    result = realloc(ptr, size);

    /* Save underlying hooks */
    OldToStandart();

#ifdef MALLOC_HOOK_LOGS
            printf("malloc_hooks logs: realloc : %p %p %lu\n", ptr, result, size);
#endif

    // Make changes in map<ptr,size>
    PtrBytesReallocTrigger(ptr, result, size);

    void *buffer[gBtBufSize];
    char **strings;
    // Get Backtrace
    int nptrs = backtrace(buffer, gBtBufSize);
    strings = backtrace_symbols(buffer, nptrs);
    if (strings == NULL)
    {
        perror("backtrace_symbols");
        exit(EXIT_FAILURE);
    }

    int end;
    if (nptrs <= gAddrCount)
        end = nptrs;
    else
        end = gAddrCount;

    std::string backtraceStr;
    for (int n = 2; n < end; n++)
    {
        backtraceStr.append(strings[n]);
        backtraceStr.append("\n");
    }
    free(strings);

    PtrBacktraceReallocTrigger(ptr, result, backtraceStr);

    /* Restore our own hooks */
    StandartToMy();
    return result;
}

void EnableHooks()
{
    my_init();
}

void PtrBytesMallocAction(void *ptr, size_t size)
{
    PtrBytes *pMap = GetPtrBytes();

#ifdef MALLOC_HOOK_LOGS
    if (pMap->find(ptr) != pMap->end())
        printf("malloc_hooks logs: ptr is already in map! just change the size\n");
    else
        printf("malloc_hooks logs: new ptr! add the size\n");
#endif

    (*pMap)[ptr] = size;
}

void PtrBytesReallocTrigger(void *ptr, void *result, size_t size)
{
    PtrBytes* pMap = GetPtrBytes();
    PtrBytes::iterator iter = pMap->find(ptr);
    if (iter != pMap->end())
    {
        if (ptr != result)
        {
#ifdef MALLOC_HOOK_LOGS
            printf("malloc_hooks logs: realloc: erase from map: %p\n", ptr);
#endif
            pMap->erase(ptr);
        }
        (*pMap)[result] = size;
    }
    else
    {
#ifdef MALLOC_HOOK_LOGS
        printf("malloc_hooks logs: warning: ptr is new address in realloc!\n");
#endif
    }
}

void PtrBytesFreeTrigger(void *ptr)
{
    PtrBytes* pMap = GetPtrBytes();
    if (pMap->find(ptr) != pMap->end())
    {
#ifdef MALLOC_HOOK_LOGS
        printf("malloc_hooks logs: free: erase from <ptr,bytes> map: %p\n", ptr);
#endif
        pMap->erase(ptr);
    }
    else
    {
#ifdef MALLOC_HOOK_LOGS
        printf("malloc_hooks logs: no such ptr in <ptr,bytes> map: %p\n", ptr);
#endif
    }
}

void PtrBacktraceMallocAction(void *ptr, std::string str)
{
    PtrBacktrace* pMap = GetPtrBacktrace();
#ifdef MALLOC_HOOK_LOGS
    if (pMap->find(ptr) != pMap->end())
        printf("malloc_hooks logs: ptr is already in backtrace map! just change the string\n");
    else
        printf("malloc_hooks logs: new ptr! add the string\n");
#endif

    (*pMap)[ptr] = str;
}

void PtrBacktraceReallocTrigger(void *ptr, void *result, std::string str)
{
    PtrBacktrace* pMap = GetPtrBacktrace();
    PtrBacktrace::iterator iter = pMap->find(ptr);
    if (iter != pMap->end())
    {
        if (ptr != result)
        {
#ifdef MALLOC_HOOK_LOGS
            printf("malloc_hooks logs: realloc: erase from backtrace map: %p\n", ptr);
#endif
            pMap->erase(ptr);
        }
#ifdef MALLOC_HOOK_LOGS
            printf("malloc_hooks logs: realloc: add to backtrace map: %p\n", ptr);
#endif
        (*pMap)[result] = str;
    }
    else
    {
#ifdef MALLOC_HOOK_LOGS
        printf("malloc_hooks logs: warning: ptr is new address in realloc!\n");
#endif
    }
}

void PtrBacktraceFreeTrigger(void *ptr)
{
    PtrBacktrace* pMap = GetPtrBacktrace();
    if (pMap->find(ptr) != pMap->end())
    {
#ifdef MALLOC_HOOK_LOGS
        printf("malloc_hooks logs: free: erase from backtrace map: %p\n", ptr);
#endif
        pMap->erase(ptr);
    }
    else
    {
#ifdef MALLOC_HOOK_LOGS
        printf("malloc_hooks logs: no such ptr in backtrace map: %p\n", ptr);
#endif
    }
}

void DumpPtrBytesMap()
{
#ifdef MALLOC_HOOK_LOGS
    PtrBytes* pMap = GetPtrBytes();
    PtrBytes::iterator iter = pMap->begin();
    printf("malloc_hooks logs: dump <ptr,bytes> map\n");
    while (iter != pMap->end())
    {
        printf("malloc_hooks logs: addr: %p, size %lu\n", iter->first, iter->second);
        ++iter;
    }
#endif
}

void DumpPtrBacktraceMap()
{
#ifdef MALLOC_HOOK_LOGS
    PtrBacktrace* pMap = GetPtrBacktrace();
    PtrBacktrace::iterator iter = pMap->begin();
    printf("malloc_hooks logs: dump <ptr,backtrace> map\n");
    while (iter != pMap->end())
    {
        printf("malloc_hooks logs: addr: %p, backtrace:\n %s\n", iter->first, iter->second.c_str());
        ++iter;
    }
#endif
}

bool sortBySize(const std::pair<std::string, std::size_t> &left, const std::pair<std::string, std::size_t> &right)
{
    return (left.second > right.second);
}

void PrintStat(int curRss, int prevRss)
{
#ifdef MALLOC_HOOK_LOGS
    printf("Print stat\n");
#endif

    MapBacktraceSize backtraceSize;

    PtrBacktrace * ptrBacktrace = GetPtrBacktrace();
    PtrBytes * ptrBytes = GetPtrBytes();

    // Iterate throught map<ptr,backtrace>
    PtrBacktrace::iterator ptrBacktraceIter = ptrBacktrace->begin();
    PtrBytes::iterator ptrBytesIter;

    void * ptr = NULL;
    std::string backtrace;

    while (ptrBacktraceIter != ptrBacktrace->end())
    {
        // Get ptr and backtrace
        ptr = ptrBacktraceIter->first;
        backtrace = ptrBacktraceIter->second;

        // Find <ptr,bytes> pair
        ptrBytesIter = ptrBytes->find(ptr);
        if (ptrBytesIter != ptrBytes->end())
        {
            // If found size in bytes by the pointer - add it to specific backtrace
            backtraceSize[backtrace] += ptrBytesIter->second;
        }
        else
        {
#ifdef MALLOC_HOOK_LOGS
            printf("<ptr,bytes> pair hasn't been found in <ptr,bytes> map\n");
#endif
        }

        ++ptrBacktraceIter;
    }

    // fill vector with <backtrace, size> pair
    VecBacktraceSize vecStat;

    MapBacktraceSize::iterator backtraceSizeIter = backtraceSize.begin();
    while (backtraceSizeIter != backtraceSize.end())
    {
        vecStat.push_back(std::make_pair(backtraceSizeIter->first, backtraceSizeIter->second));
        ++backtraceSizeIter;
    }

    std::sort(vecStat.begin(), vecStat.end(), sortBySize);

#ifdef MALLOC_HOOK_LOGS
    printf("Print stat vector size %lu:\n", vecStat.size());
    printf("Print stat backtrace, size:\n");

    for (int i = 0; i < vecStat.size(); ++i)
    {
        printf("%s\t\t%lu\n", vecStat[i].first.c_str(), vecStat[i].second);
    }
#endif

#ifdef MALLOC_HOOK_TO_FILE_LOGS
    std::string file = gLogFileName;
    char fileCount[4];
    sprintf(fileCount, "%d", gLogFileCount);
    ++gLogFileCount;
    file.insert(file.find("."), fileCount);

    FILE * pFile;
    pFile = fopen(file.c_str(), "w");
    if (!pFile)
        exit(EXIT_FAILURE);

    fprintf(pFile, "RSS stat: current = %d, prev = %d\nBacktraces: %d\n", curRss, prevRss, vecStat.size());
    for (int i = 0; i < vecStat.size(); ++i)
        fprintf(pFile, "-----------------------------------\nAllocated size: %lu\nBacktrace:\n %s", vecStat[i].second, vecStat[i].first.c_str());
    gRss = curRss;
    fclose(pFile);
#endif
}

int GetProcessRss()
{
    char info[128];
    FILE *fp = popen("ps -o rss $(pidof ...)", "r");
    if (!fp)
        return 0;

    int value;
    while (fgets(info, 128, fp) != NULL); // get last row
    sscanf(info, "%d", &value);
    fclose(fp);

    return value;
}

void CheckMemoryConsumption()
{
    int curRss = GetProcessRss();
    PrintStat(curRss, gRss);
}
